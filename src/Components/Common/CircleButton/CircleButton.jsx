import React from "react"
import "./CircleButton.scss"

export const CircleButton = (props) => {
    return (
    <div className={"rootCircle"} onClick={props.onClick}>
        {props.value}
    </div>
    )
}