import React from "react"
import "./Footer.scss"
import { GAME_STATE } from "../../../Enums"

export const Footer = (props) => {
    return (
        <div className={"boardfoot"}>
            {props.gameState === GAME_STATE.LOST && <p className={"lostMessage"}>You lost</p>}
            {props.gameState === GAME_STATE.WON && <p className={"wonMessage"}>You won</p>}
            {props.gameState === GAME_STATE.ON_GAME && <p></p>}
            <div className={"score"}>
                <p className={"movesLeftMessage"}>Moves left: </p>
                <p>{props.maxMoves}</p>
            </div>
        </div>
    )
}