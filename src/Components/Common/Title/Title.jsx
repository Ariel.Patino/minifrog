import React from "react"
import "./Title.scss"

export const Title = (props) => {
    return (
        <div className={`rootTitle ${props.classTitles}`}>
            {props.value}
        </div>
    )
}
 

