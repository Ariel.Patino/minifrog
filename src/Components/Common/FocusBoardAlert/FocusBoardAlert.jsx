import React from "react"
import "./FocusBoardAlert.scss"

export const FocusBoardAlert = (props) => {
    return (
    <div className={"userAlert blink_me"}>
        {!props.focusGame && `Click On Board to Keep Playing`} 
    </div>
    )
}