import React, { Component } from "react"
import "./Game.scss"
import { Board } from "../Board";
import { RemoveCharactersFromString } from "../../Helper"
import { GAME_STATE } from "../../Enums"
import { Title } from "../Common/Title";
import { Footer } from "../Common/Footer";
import { FocusBoardAlert } from "../Common/FocusBoardAlert";
import { CircleButton } from "../Common/CircleButton";


export class Game extends Component {
    constructor(props) {
        super(props);
        this.state = this.initializeGame();
    }

    initializeGame = () => {
        return {
            rows: process.env.REACT_APP_ROWS,
            cols: process.env.REACT_APP_COLUMNS,
            wallsPositions: process.env.REACT_APP_WALL_POSITION,
            maxMoves: process.env.REACT_APP_MAX_MOVES,
            startPosition: process.env.REACT_APP_START_POSITION,
            endPosition: process.env.REACT_APP_END_POSITION,
            currentPosition: process.env.REACT_APP_START_POSITION,
            gameState: GAME_STATE.ON_GAME,
            focusGame: false,
            level: 1
        }
    }

    moveFrog = (axe, blocksToAdd) => {
        const { currentPosition, rows, cols } = this.state;
        const coordinates = RemoveCharactersFromString(["{", "}"], currentPosition).split(".");
        switch (axe) {
            case "row":
                if (this.isNextBlockAWall(parseInt(coordinates[0]) + parseInt(blocksToAdd), parseInt(coordinates[1]))) {
                    break;
                }
                if (this.isNextBlockOutOfLimit(parseInt(coordinates[0]), parseInt(blocksToAdd), rows)) {
                    break;
                }
                this.updateFrogPosition(parseInt(coordinates[0]) + parseInt(blocksToAdd), parseInt(coordinates[1]));
                this.discountMoves();
                break;
            case "col":
                if (this.isNextBlockAWall(parseInt(coordinates[0]), parseInt(coordinates[1]) + blocksToAdd)) {
                    break;
                }
                if (this.isNextBlockOutOfLimit(parseInt(coordinates[1]), parseInt(blocksToAdd), cols)) {
                    break;
                }
                this.updateFrogPosition(parseInt(coordinates[0]), parseInt(coordinates[1]) + blocksToAdd);
                this.discountMoves();
                break;
            default:
                console.warn("Only row or col values");
        }
    }

    isNextBlockAWall = (col, row) => {
        const { wallsPositions } = this.state;
        const wallCoordinates = RemoveCharactersFromString(["[", "]"], wallsPositions).split(",");
        return !!wallCoordinates.find(coords => coords === `{${row}.${col}}`);
    }

    isNextBlockOutOfLimit = (currentValue, blocksToAdd, limit) => {
        return currentValue + blocksToAdd < 0 ? true :
            currentValue + blocksToAdd > limit - 1 ? true :
                false;
    }

    discountMoves = () => {
        const { maxMoves } = this.state;
        this.setState({
            maxMoves: maxMoves - 1
        });
        this.updateGameState();
    }

    updateFrogPosition(col, row) {
        this.setState({
            currentPosition: `{${col}.${row}}`
        })
        this.updateGameState(`{${col}.${row}}`);
    }

    updateGameState = (currentPosition) => {
        const { maxMoves, endPosition } = this.state;
        if (currentPosition === endPosition) {
            this.setState({
                gameState: GAME_STATE.WON
            });
        } else if (maxMoves - 1 === 0 && currentPosition === endPosition) {
            this.setState({
                gameState: GAME_STATE.WON
            });
        } else if (maxMoves - 1 === 0) {
            this.setState({
                gameState: GAME_STATE.LOST
            });
        }
    }

    handleKeyDown = (e) => {
        if (this.keepOnGame()) {
            switch (e.keyCode) {
                case 37: this.moveFrog("col", -1);
                    break;
                case 38: this.moveFrog("row", -1);
                    break;
                case 39: this.moveFrog("col", 1);
                    break;
                case 40: this.moveFrog("row", 1);
                    break;
                default:
                    console.warn("Only Cursors are accepted");
            }
        }
    }

    handleKeyPress = (e) => {
        const { gameState } = this.state;
        if (gameState === GAME_STATE.LOST ||
            gameState === GAME_STATE.WON) {
            switch (e.charCode) {
                case 13:
                    this.handleRestar();
                    break;
                default:
                    console.warn("Action Not Implemented");
            }
        }
    }

    keepOnGame = () => {
        return this.state.gameState === GAME_STATE.ON_GAME;
    }

    focusGame = () => {
        this.setState({
            focusGame: true
        });
    }

    blurGame = () => {
        this.setState({
            focusGame: false
        });
    }

    handleRestar = () => {
        let restarState = this.initializeGame();
        restarState.focusGame = true;
        this.setState(restarState);
    }

    handleClickNextLevel = () => {
        this.setState({
            level: this.state.level + 1
        })
    }

    render() {
        const {
            rows,
            cols,
            wallsPositions,
            startPosition,
            endPosition,
            currentPosition,
            maxMoves,
            gameState,
            focusGame,
            level } = this.state;
        return (
            <div className={"rootGame"}
                onKeyDown={this.handleKeyDown}
                onKeyPress={this.handleKeyPress}
                tabIndex="0"
                onFocus={this.focusGame}
                onBlur={this.blurGame}>
                <Title value={"Mini Frog"} />
                <div className={"boardBody"}>
                    <Board
                        rows={rows}
                        cols={cols}
                        walls={wallsPositions}
                        startPosition={startPosition}
                        endPosition={endPosition}
                        currentPosition={currentPosition}
                        gameState={gameState}
                        level={level} />
                </div>
                <Footer gameState={gameState} maxMoves={maxMoves} />
                <div className={"options"}>
                    <CircleButton value={"Next Level"} onClick={this.handleClickNextLevel} />
                    <CircleButton value={"Restar"} onClick={this.handleRestar} />
                </div>
                <FocusBoardAlert focusGame={focusGame} />
            </div>
        )
    }

}