import React  from 'react';
import "./Frog.scss"
import frog from "../../Resources/Images/frog.png";
import sadFrog from "../../Resources/Images/frogSad.png";
import happyFrog from "../../Resources/Images/happyFrog.png";
import {GAME_STATE} from "../../Enums"

export const Frog = (props) => {
    return (
        <div className={"rootFrog"} >
            <img src={
                    props.gameState === GAME_STATE.LOST ? sadFrog :
                    props.gameState === GAME_STATE.WON ? happyFrog :
                    frog } alt="frog" />
        </div>
    )
} 

