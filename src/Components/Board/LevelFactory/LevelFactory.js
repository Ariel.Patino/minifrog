import { RemoveCharactersFromString } from "../../../Helper"

export const LevelFactory = (level, rows, columns, levelProperties) => {
    let result = [];
    let columnsTemp = [];

    switch (level) {
        case 1: 
            for (let indexRow = 0; indexRow < rows; ++indexRow) {
                for (let indexCol = 0; indexCol < columns; ++indexCol) {
                    drawBlock(columnsTemp, indexRow, indexCol, levelProperties)
                }
                result.push(columnsTemp);
                columnsTemp = [];
            }
            break;
        default: 
            result = [];
    }
    return result;
}


const drawBlock = (columns, row, col, levelProperties) => {
    let blockType = 0;
    if (isWall(row, col, levelProperties)) {
        blockType = 1;
    } else if (isStartPosition(row, col, levelProperties)) {
        blockType = 2;
    } else if (isEndPosition(row, col, levelProperties)) {
        blockType = 3;
    }
    columns.push(blockType);
}

const isWall = (row, column, levelProperties) => {
    const {walls} = levelProperties;
    const tempArray = RemoveCharactersFromString(["[", "]"], walls).split(",");
    return tempArray.find(point => point === `{${row}.${column}}`)
}

const isStartPosition = (row, column, levelProperties) => {
    const { startPosition } = levelProperties;
    const positions = RemoveCharactersFromString(["{", "}"], startPosition).split(".");
    return parseInt(positions[0]) === column && parseInt(positions[1]) === row;
}

const isEndPosition = (row, column, levelProperties) => {
    const { endPosition } = levelProperties;
    const positions = RemoveCharactersFromString(["{", "}"], endPosition).split(".");
    return parseInt(positions[0]) === column && parseInt(positions[1]) === row;
}

