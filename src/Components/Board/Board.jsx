import React from "react"
import "./Board.scss"

import classNames from "classnames"
import { Frog } from "../Frog";
import { LevelFactory } from "./LevelFactory"
import { Title } from "../Common/Title";
import sleptFrog from "../../Resources/Images/sleptFrog.png";


export class Board extends React.Component {

    board = [];
    constructor(props) {
        super(props);
        this.state = {
            topLevel: 1
        }
    }

    boardCreation = (level) => {
        const { cols, rows, walls, startPosition, endPosition} = this.props;
        this.board = LevelFactory(
            level, 
            cols, 
            rows,
            {
                walls: walls,
                startPosition: startPosition,
                endPosition: endPosition
            });
        
        
    }

    analizeFrogPosition = (col, row) => {
        const { currentPosition } = this.props;
        return currentPosition === `{${col}.${row}}`;
    }


    render() {
        const { cols, rows, gameState, level } = this.props;
        const { topLevel } = this.state;
        this.boardCreation(level);
        return <div className={"root"} style={{ gridTemplateColumns: `repeat(${cols}, 50px)`, gridTemplateRows: `repeat(${rows}, 50px)` }}>
            { 
                level <= topLevel ?
                this.board.map((row, indexRow) => {
                    return (<div key={`row-${indexRow}`}>{
                        row.map((boardValue, indexColumn) => {
                            return (<div key={`block-${indexRow}-${indexColumn}`}
                                className={classNames(
                                    boardValue === 0 && "freeBlock",
                                    boardValue === 1 && "wall",
                                    boardValue === 2 && "frog",
                                    boardValue === 3 && "goal",
                                    "block"
                                )
                                }
                            >
                                {this.analizeFrogPosition(indexColumn, indexRow) &&
                                    <Frog gameState={gameState} />}
                            </div>)
                        })
                    }</div>)
                }) :
                <div className={"missLevel"}>
                    <Title value={"IN PROGRESS"} classTitles={"error"}/>
                    <img src={sleptFrog} alt="frog" />
                </div>
            }
        </div>
    }
} 