import React from "react";
import { shallow } from "enzyme";
import { Frog } from "../../Components/Frog";
test("should test Frog component", () => {
 const wrapper = shallow(<Frog />);
 expect(wrapper).toMatchSnapshot();
});