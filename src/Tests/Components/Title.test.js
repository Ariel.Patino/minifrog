import React from "react";
import { shallow } from "enzyme";
import { Title } from "../../Components/Common/Title";

test("should test Title component", () => {
 const wrapper = shallow(<Title />);
 expect(wrapper).toMatchSnapshot();
});
