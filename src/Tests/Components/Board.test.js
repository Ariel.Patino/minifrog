import React from "react";
import { shallow } from "enzyme";
import { Board } from "../../Components/Board";
import { LevelFactory } from "../../Components/Board/LevelFactory";


test("should test Board component", () => {
 const wrapper = shallow(<Board />);
 expect(wrapper).toMatchSnapshot();
});

test("should test Board component without Walls ", () => {
    const wrapper = shallow(<Board walls="[]"/>);
    expect(wrapper).toMatchSnapshot();
});

test("should test Board component wit Walls more than board can accept", () => {
    const wrapper = shallow(<Board 
                                walls="[{0.0},{0.1},{1.0},{1.1},{2.2},]" 
                                rows="2"
                                cols="2"/>);
    expect(wrapper).toMatchSnapshot();
});

test("should test Board component Levels Creation", () => {
    const wrapper = LevelFactory(1, 2, 2, {walls:"{0.1}", startPosition:"{0.0}", endPosition:"{5.5}"});
    expect(wrapper).toStrictEqual([[ 2, 1], [ 0,0]]);
});

test("should test Board component Levels Creation", () => {
    const wrapper = LevelFactory(1, 3, 2, {walls:"{0.1}", startPosition:"{0.0}", endPosition:"{5.5}"});
    expect(wrapper).toStrictEqual([[ 2, 1],[0,0],[0,0]]);
});