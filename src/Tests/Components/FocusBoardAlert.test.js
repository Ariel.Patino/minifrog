import React from "react";
import { shallow } from "enzyme";
import { FocusBoardAlert } from "../../Components/Common/FocusBoardAlert";

test("should test FocusBoardAlert component", () => {
 const wrapper = shallow(<FocusBoardAlert />);
 expect(wrapper).toMatchSnapshot();
});
