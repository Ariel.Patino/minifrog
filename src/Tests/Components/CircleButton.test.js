import React from "react";
import { shallow } from "enzyme";
import { CircleButton } from "../../Components/Common/CircleButton";

test("should test CircleButton.test component", () => {
 const wrapper = shallow(<CircleButton />);
 expect(wrapper).toMatchSnapshot();
});
