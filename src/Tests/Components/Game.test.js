import React from "react";
import { shallow } from "enzyme";
import { Game } from "../../Components/Game";

test("should test Game component", () => {
 const wrapper = shallow(<Game />);
 expect(wrapper).toMatchSnapshot();
});

test("should call handleKeyDown on Game component with key upKey", () => {
    const wrapper = shallow(<Game />);
    const instance = wrapper.instance();
    instance.handleKeyDown({keyCode:38})
    expect(wrapper).toMatchSnapshot();
});

test("should call handleKeyDown on Game component with key leftKey", () => {
    const wrapper = shallow(<Game />);
    const instance = wrapper.instance();
    instance.handleKeyDown({keyCode:37})
    expect(wrapper).toMatchSnapshot();
});

test("should call handleKeyDown on Game component with key downKey", () => {
    const wrapper = shallow(<Game />);
    const instance = wrapper.instance();
    instance.handleKeyDown({keyCode:40})
    expect(wrapper).toMatchSnapshot();
});

test("should call handleKeyDown on Game component with key rightKey", () => {
    const wrapper = shallow(<Game />);
    const instance = wrapper.instance();
    instance.handleKeyDown({keyCode:39})
    expect(wrapper).toMatchSnapshot();
});

test("should call handleKeyDown on Game component with key any", () => {
    const wrapper = shallow(<Game />);
    const instance = wrapper.instance();
    instance.handleKeyDown({keyCode:9})
    expect(wrapper).toMatchSnapshot();
});


test("should call handleKeyPress on Game component with key Enter", () => {
    const wrapper = shallow(<Game />);
    const instance = wrapper.instance();
    instance.handleKeyPress({keyCode:13})
    expect(wrapper).toMatchSnapshot();
});

test("should call handleKeyPress on Game component with key Esc", () => {
    const wrapper = shallow(<Game />);
    const instance = wrapper.instance();
    instance.handleKeyPress({keyCode:27})
    expect(wrapper).toMatchSnapshot();
});

test("should call handleKeyPress on Game component with key Enter With Lost State", () => {
    const wrapper = shallow(<Game />);
    const instance = wrapper.instance();
    for(let i = instance.state.maxMoves; i >= 0; --i) {
        instance.discountMoves();
    }
    instance.handleKeyPress({keyCode:13})
    expect(wrapper).toMatchSnapshot();
});